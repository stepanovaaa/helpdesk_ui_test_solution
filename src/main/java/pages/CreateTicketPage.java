package pages;

import io.qameta.allure.Step;
import models.Dictionaries;
import models.Ticket;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.io.File;

/** Страница создания тикета */
public class CreateTicketPage extends HelpdeskBasePage {

    @FindBy(xpath = "//select[@name='queue']")
    private WebElement selectQueue;

    @FindBy(xpath = "//input[@name='title']")
    private WebElement inputProblem;

    @FindBy(xpath = "//textarea[@name='body']")
    private WebElement textareaDescription;

    @FindBy(xpath = "//select[@name='priority']")
    private WebElement selectPriority;

    @FindBy(xpath = "//input[@name='due_date']")
    private WebElement inputDueDate;

    @FindBy(xpath = "//input[@id='id_attachment']")
    private WebElement inputAttachment;

    @FindBy(xpath = "//input[@name='submitter_email']")
    private WebElement inputSubmitter_email;

    @FindBy(xpath = "//button[@type='submit']")
    private WebElement submitTicketButton;

    public CreateTicketPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Создать тикет")
    public CreateTicketPage createTicket(Ticket ticket) {
        setSelectQueue(ticket.getQueue());
        setInputProblem(ticket.getTitle());
        setTextareaDescription(ticket.getDescription());
        setSelectPriority(ticket.getPriority());
        setInputDueDate(ticket.getDue_date());
        setInputAttachment(ticket.getFile());
        setInputSubmitter_email(ticket.getSubmitter_email());
        clickOnSubmitButton();
        return this;
    }

    @Step("Ввести имя проблемы: {text}")
    public void setInputProblem(String text) {
        inputProblem.sendKeys(text);
    }

    @Step("Выбрать очередь: {queue}")
    public void setSelectQueue(Integer queue) {
        Select select = new Select(selectQueue);
        select.selectByVisibleText(Dictionaries.getQueue(queue));
    }

    @Step("Ввести описание проблемы: {description}")
    public void setTextareaDescription(String description) {
        textareaDescription.sendKeys(description);
    }

    @Step("Выбрать приоритет проблемы: {priority}")
    public void setSelectPriority(Integer priority) {
        Select select = new Select(selectPriority);
        select.selectByVisibleText(Dictionaries.getPriority(priority));
    }

    @Step("Ввести дату: {dueDate}")
    public void setInputDueDate(String dueDate) {
        inputDueDate.sendKeys(dueDate);
    }

    @Step("Добавить файл/скриншот: {file}")
    public void setInputAttachment(File file) {
        inputAttachment.sendKeys(file.getAbsolutePath());
    }

    @Step("Ввести электронный адрес: {submitter_email}")
    public void setInputSubmitter_email(String submitter_email) {
        inputSubmitter_email.sendKeys(submitter_email);
    }

    @Step("Нажать на кнопку создания тикета")
    public void clickOnSubmitButton() {
        submitTicketButton.click();
    }
}

package pages;

import io.qameta.allure.Step;
import models.Dictionaries;
import models.Ticket;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

/** Страница отдельного тикета (авторизированный пользователь) */
public class TicketPage extends HelpdeskBasePage {

    /* Верстка страницы может измениться, поэтому для таблиц вместо индексов строк и столбцов лучше использовать
       более универсальные локаторы, например поиск по тексту + parent, following-sibling и другие.

       Текст тоже может измениться, но в этом случае элемент не будет найден и тест упадет,
       а ошибку можно будет легко локализовать и исправить.
       В случае изменений ячеек таблицы, локатор будет продолжать работать, но будет указывать на другой элемент,
       поведение теста при этом изменится непредсказуемым образом и ошибку будет сложно найти. */
    private WebElement dueDate = driver.findElement(By.xpath("//input[@name='due_date']"));

    // проинициализировать элементы через driver.findElement
    private WebElement title = driver.findElement(By.name("title"));
    private WebElement queue = driver.findElement(By.xpath("//th[contains(text(), 'Queue: ')]"));
    private WebElement email = driver.findElement(By.xpath("//th[contains(text(), 'Submitter E-Mail')]/following-sibling::td"));
    private WebElement priority = driver.findElement(By.xpath("//th[contains(text(), 'Priority')]/following-sibling::td"));
    private WebElement description = driver.findElement(By.xpath("//td[@id='ticket-description']/p"));

    @Step("Проверить значение полей на странице тикета")
    public void checkTicket(Ticket ticket) {
        // Извлечение значений полей на странице тикета
        String actualTitle = title.getAttribute("value");
        // Делим текст по строка, берем вторую, удаляем лишнее
        String actualQueue = queue.getText().split("\n")[1].replace("Queue: ","").trim();
        String actualEmail = email.getText();
        String actualPriority = priority.getText();
        String actualDescription = description.getText();

        // Проверка, что значения на странице совпадают с ожидаемыми значениями из объекта Ticket
        Assert.assertEquals(ticket.getTitle(), actualTitle, "Неверное значение поля Title");
        Assert.assertEquals(Dictionaries.getQueue(ticket.getQueue()), actualQueue, "Неверное значение поля Queue");
        Assert.assertEquals(ticket.getSubmitter_email(), actualEmail, "Неверное значение поля Email");
        Assert.assertEquals(Dictionaries.getPriority(ticket.getPriority()), actualPriority, "Неверное значение поля Priority");
        Assert.assertEquals(ticket.getDescription(), actualDescription, "Неверное значение поля Description");
    }
}

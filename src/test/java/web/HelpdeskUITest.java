package web;

import elements.MainMenu;
import io.qameta.allure.Step;
import models.Ticket;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.*;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class HelpdeskUITest {

    private WebDriver driver;
    private Ticket ticket;

    @BeforeClass
    public void setup() throws IOException {
        loadProperties();
        setupDriver();
    }

    @Step("Загрузить конфигурационные файлы")
    private void loadProperties() throws IOException {
        // Читаем конфигурационные файлы в System.properties
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("config.properties"));
        System.getProperties().load(ClassLoader.getSystemResourceAsStream("user.properties"));
    }

    @Step("Создать экземпляр драйвера")
    private void setupDriver() {
        String chromeDriverPath = System.getProperty("webdriver.chrome.driver");
        System.setProperty("webdriver.chrome.driver", chromeDriverPath);
        // Создание экземпляра драйвера
        driver = new ChromeDriver();
        // Устанавливаем размер окна браузера, как максимально возможный
        driver.manage().window().maximize();
        // Установим время ожидания для поиска элементов
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        // Установить созданный драйвер для поиска в веб-страницах
        AbstractPage.setDriver(driver);
    }

    @Test
    public void createTicketTest() {
        // шаги тест-кейса
        // Шаг 1
        // Нажать на пункт "New ticket" в главном меню
        openNewTicketsPage();

        // Шаг 2
        // Создать тикет
        CreateTicketPage createTicketPage = new CreateTicketPage();
        ticket = buildNewTicket(); // Создание тикета
        createTicketPage.createTicket(ticket);
        ViewPage viewPage = new ViewPage();
        viewPage.checkTicket(ticket);
        viewPage.saveId(ticket);

        // Шаг 3
        // Нажать на кнопку "Log In"
        MainMenu mainPage = new MainMenu(driver);
        mainPage.clickOnLogInButton();

        // Шаг 4
        // Выполнить авторизацию
        LoginPage loginPage = new LoginPage();
        String username = System.getProperty("user");
        String password = System.getProperty("password");
        loginPage.login(username, password);
        String actualUser = mainPage.loginedUser();
        Assert.assertEquals(actualUser, username, "На кнопке логина не отображается имя пользователя");

        // Шаг 5
        // Найти созданный тикет
        mainPage.searchTicket(ticket);

        // Шаг 6
        // Нажать на ссылку созданного тикета в столбце Ticket
        TicketsPage ticketsPage = new TicketsPage();
        ticketsPage.openTicket(ticket);
        TicketPage ticketPage = new TicketPage();
        ticketPage.checkTicket(ticket);

    }

    private Ticket buildNewTicket() {
        Ticket ticket = new Ticket();
        // заполнить поля тикета
        ticket.setTitle("Тикет для тестов");
        ticket.setQueue(1);
        ticket.setDescription("Описание проблемы");
        ticket.setPriority(3);
        ticket.setDue_date("2023-12-31");
        ticket.setFile(new File("./pom.xml"));
        ticket.setSubmitter_email("tickettest@example.com");
        return ticket;
    }

    private void openNewTicketsPage(){
        String URL = System.getProperty("site.url");
        driver.get(URL);
        //Переход на страницу создания тикета
        driver.findElement(By.xpath("//span[contains(text(),'New Ticket')]")).click();
    }


    @AfterTest
    public void close() {
        if (driver != null) {
            // Закрываем одно текущее окно браузера
            driver.close();
            // Закрываем все открытые окна браузера, завершаем работу браузера, освобождаем ресурсы
           driver.quit();
        }
    }
}
